#include <stddef.h>
#include <stdint.h>

uint32_t
crc24(const char *data, size_t length)
{
    uint32_t crc = 0;
    while (length--) {
        crc ^= (*data++) << 16;
        for (int i = 0; i < 8; i++) {
            crc = (crc << 1) ^ (-((crc >> 23) & 1) & 0x800063);
        }
    }
    return crc & 0xFFFFFF;
}
