#ifndef OS9_CRC_H
#define OS9_CRC_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stddef.h>
#include <stdint.h>

uint32_t crc24(const char *data, size_t length);

#ifdef __cplusplus
}
#endif

#endif
