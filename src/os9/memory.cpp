#include "memory.h"
#include <optional>

std::optional<os9::mem_pointer>
os9::ColoredMemory::obtain_block(unsigned int size)
{
    os9::mem_pointer ptr = 0;
    auto it = this->free_memory.begin();
    while (it != this->free_memory.end()) {
        if (it->size >= size) {
            ptr = it->position;
            it->size -= size;
            it->position += size;
            if (!it->size) {
                this->free_memory.erase(it);
            }
            return ptr;
        }
    }
    return {};
}

void
os9::ColoredMemory::put_back_block(os9::mem_pointer pos, unsigned int size)
{
    auto it = this->free_memory.begin();
    while (it != this->free_memory.end()) {
        if (it->position > (pos + size)) {
            break;
        }
    }

    this->free_memory.emplace(it, pos, size);
}

os9::mem_pointer
os9::MemoryContext::allocate(unsigned int size)
{
    auto it = this->colored_memorys.begin();

    while (it != this->colored_memorys.end()) {
        if (!it->empty()) {
            std::optional<os9::mem_pointer> ptr
                = it->obtain_block(size / OS9_BLOCK_SIZE);
            if (!ptr.has_value()) {
                it++;
                continue;
            }
            return (*ptr * OS9_BLOCK_SIZE) + it->base_address;
        }
        it++;
    }
    return 0;
}

void
os9::MemoryContext::deallocate(os9::mem_pointer pointer, unsigned int size)
{
    auto it = this->colored_memorys.begin();

    while (it != this->colored_memorys.end()) {
        if (it->base_address <= pointer
            && (it->base_address + it->size) >= (pointer + size)) {
            it->put_back_block((pointer - it->base_address) / OS9_BLOCK_SIZE,
                               size / OS9_BLOCK_SIZE);
            break;
        }
    }
}

void
os9::MemoryContext::add_colored_memory(os9::mem_pointer base_address,
                                       uint32_t blocks, uint16_t priority)
{
    ColoredMemory memory;
    memory.priority = priority;
    memory.base_address = base_address;
    memory.size = blocks * OS9_BLOCK_SIZE;

    memory.free_memory.push_back({ 0, blocks });
    this->colored_memorys.push_back(memory);
}
