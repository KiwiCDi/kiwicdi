#ifndef OS9_MEMORY_H
#define OS9_MEMORY_H

#include <cstdint>
#include <deque>
#include <list>
#include <memory>
#include <optional>

#define OS9_BLOCK_SIZE 16

namespace os9 {
typedef uint32_t mem_pointer;

struct FreeNode {
    os9::mem_pointer position;
    unsigned int size;
};

class ColoredMemory {
    std::list<struct FreeNode> free_memory;
    os9::mem_pointer base_address;
    uint32_t size;
    uint16_t priority;

    friend class MemoryContext;

  protected:
    std::optional<unsigned int> obtain_block(unsigned int size);
    void put_back_block(os9::mem_pointer pos, unsigned int size);

  public:
    inline bool
    empty()
    {
        return free_memory.empty();
    };
};

class MemoryContext {
    std::deque<ColoredMemory> colored_memorys;
    std::shared_ptr<unsigned char[]> mem;
    uint32_t size;

  public:
    /**
     * Initialize a memory context for an OS-9 emulation context.
     * @param mem A shared pointer to block of memory for the memory context
     * @param size Size of the block divided by 16 bytes
     */

    MemoryContext(std::shared_ptr<unsigned char[]> mem, uint32_t size)
        : mem(mem), size(size) {};
    os9::mem_pointer allocate(unsigned int size);

    /**
     * Deallocate a section of memory
     *
     * @note OS-9 will mindlessly trust whatever you put into the parameters,
     *      Please do not put invalid parameters.
     * @param pointer A memory address to deallocate.
     * @param size Size of the memory block.
     */
    void deallocate(os9::mem_pointer pointer, uint32_t size);
    void add_colored_memory(os9::mem_pointer base_address, uint32_t blocks,
                            uint16_t priority);

    template <typename type> type read(os9::mem_pointer position);
    template <typename type> void write(os9::mem_pointer position, type data);
};
};
#endif
