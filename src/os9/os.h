#ifndef OS9_OS_H
#define OS9_OS_H

#include "os9/memory.h"
#include <map>
#include <string>
namespace os9 {

class System {
    std::map<std::string, os9::mem_pointer> modules;
    MemoryContext mem_ctx;

  public:
    System(MemoryContext mem_ctx) : mem_ctx(mem_ctx) {};
};

}
#endif
